trigger CaseUpdatedTrigger on Case (before update) {
    List<Case> caseList = trigger.new;
    Set<Id> contactId = new Set<Id>();
    List<Hire_Form__c> hireList = new List<Hire_Form__c>();
    Case caseObject;
    for(Integer i=0; i<caseList.size(); i++){
        caseObject = caseList.get(i);
        if(caseObject.Status == 'Closed'){
            contactId.add(caseObject.ContactId);
            //System.debug('cont id ==> ' + contactId);
        }
    }
    
    hireList = [SELECT Id, Status__c FROM Hire_Form__c WHERE Candidate__c IN: contactId];   
    //System.debug('hire list ==> ' + hireList);
    for(Hire_Form__c hire : hireList){
        if(hire.Status__c != 'Completed'){
            //System.debug('hire status ==> ' + hire.Status__c);
            System.debug('case obj ==> ' + caseObject);
            caseObject.Status.addError('You can not close the case until hire form  is completed');             
        }      
    }
}