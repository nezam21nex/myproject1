import { LightningElement, wire, track} from 'lwc';
import fetchContactCount from '@salesforce/apex/AccountContactCountInLWCPage.fetchContactCount';

export default class AccountContactCount extends LightningElement {
		
		@wire (fetchContactCount)
		accounts
		
		isChecked = false;
		
		checkboxHandler(){
				this.isChecked = !this.isChecked;
				
		}
		
	
}