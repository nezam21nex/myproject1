import { LightningElement, wire, api, track } from 'lwc';
import { refreshApex } from '@salesforce/apex';
import GET_ACCOUNT from '@salesforce/apex/AccountDetailsFetchController.getAccounts';
//import NAME_FIELD from '@salesforce/schema/Account.Name';
//import REVENUE_FIELD from '@salesforce/schema/Account.Type';
//import INDUSTRY_FIELD from '@salesforce/schema/Account.Industry';

export default class AccountDetails extends LightningElement {
    @api accountData;
    @api recordId;
    @track imageUrl;
    @track refreshAccount;
    //fields = [NAME_FIELD, REVENUE_FIELD, INDUSTRY_FIELD];
    isShowModel = false;

    // @wire (GET_ACCOUNT, {accId : '$recordId'})
    // accounts

    //eval("$A.get('e.force:refreshView').fire();");

    @wire(GET_ACCOUNT, { accountId: '$recordId' })
    accounts(res) {
        this.refreshAccount = res;
        if (res.error) {
            console.log("error", res.error);
        } else if (res.data) {
            console.log("data", res.data);
            this.accountData = res.data[0];
            console.log("accountData", this.accountData);
            this.imageUrl = '/sfc/servlet.shepherd/version/download/' + res.data[0].pictures;
            console.log("imageUrl", this.imageUrl);
        }
    }

    connectedCallback() {
        console.log('recordId ==> ', this.recordId);
        console.log('account data ==> ', this.accountData);
    }
    handleEdit() {
        this.isShowModel = !this.isShowModel;
        refreshApex(this.refreshAccount);
    }
    handleSubmit(event) {
        //event.preventDefault(); // stop the form from submitting
        const fields = event.detail.fields;
        console.log('fields ==> ', fields);
        //fields.LastName = 'My Custom Last Name'; // modify a field
        this.template.querySelector('lightning-record-form').submit(fields);
    }

}